/*
 * AppController.j
 * MyUOW
 *
 * Created by James Wilson (timeimp) on July 20, 2012.
 * Copyright 2012, Your Company All rights reserved.
 */

@import <TNKit/TNKit.j>

@implementation UOWSubjectView : CPView
{
		TNTabView	_subjectModulesTabView;
		CPTextField _subjectTitle 			@accessors(property=subjectTitle);
		CPTextField _subjectDescription		@accessors(property=subjectDescription);
}

-(id)initWithFrame:(CGRect)aFrame{
	self = [super initWithFrame:aFrame];
	if(self){

		[self setAutoresizesSubviews:YES];
		_subjectModulesTabView = [[TNTabView alloc] initWithFrame:CGRectMakeZero()];
		//[_subjectModulesTabView setAutoResizingMask: CPViewHeightSizable | CPViewWidthSizable];
		[_subjectModulesTabView setFrameSize:CGSizeMake(CGRectGetWidth(aFrame), CGRectGetHeight(aFrame))];
		[_subjectModulesTabView setContentBackgroundColor:[CPColor grayColor]];
		[self addSubview:_subjectModulesTabView];
		[self setBackgroundColor:[CPColor greenColor]];

		var myView = [[CPView alloc] initWithFrame:CGRectMakeZero()];
		[myView setBackgroundColor:[CPColor whiteColor]];
		[myView setFrame:CGRectMake(0,0,CGRectGetWidth(aFrame), CGRectGetHeight(aFrame)-24)];

		subjectTitle = [[CPTextField alloc] initWithFrame:CGRectMakeZero()];
		[subjectTitle setStringValue:@"<<Subject Title Not Defined>>"];
		[subjectTitle setFont:[CPFont boldSystemFontOfSize:18.0]];

		subjectDescription = [[CPTextField alloc] initWithFrame:CGRectMakeZero()];
		[subjectDescription setStringValue:@"<<Subject Description Not Defined>>"];

		[subjectTitle sizeToFit];
		[subjectDescription sizeToFit];

		[myView addSubview:subjectTitle];
		[subjectTitle setFrameOrigin:CGPointMake(5,5)];

		[myView addSubview:subjectDescription];
		[subjectDescription setFrameOrigin:CGPointMake(5,CGRectGetMaxY([subjectTitle frame])+5)];

		var tab1 = [[CPTabViewItem alloc] initWithIdentifier:@"subjectInfoTab"];
		[tab1 setLabel:@"Subject Information"];
		[tab1 setView:myView];

		var tab2 = [[CPTabViewItem alloc] initWithIdentifier:@"subjectLecturesTab"];
		[tab2 setLabel:@"Lecture Slides and Notes"];
		[tab2 setView:nil];

		var tab3 = [[CPTabViewItem alloc] initWithIdentifier:@"subjectDiscussionsTab"];
		[tab3 setLabel:@"Discussions"];
		[tab3 setView:nil];

		var tab4 = [[CPTabViewItem alloc] initWithIdentifier:@"subjectAssignmentTab"];
		[tab4 setLabel:@"Assignments"];
		[tab4 setView:nil];

		var tab5 = [[CPTabViewItem alloc] initWithIdentifier:@"subjectPersonalTab"];
		[tab5 setLabel:@"Messages"];
		[tab5 setView:nil];

		var tab6 = [[CPTabViewItem alloc] initWithIdentifier:@"subjectTutorialTab"];
		[tab6 setLabel:@"Tutorials and Labwork"];
		[tab6 setView:nil];

		var tabArray = [[CPArray alloc] initWithObjects:[tab1, tab2, tab3, tab4, tab5, tab6] count:6];
		for(a=0; a < [tabArray count];a++){
			var b = [tabArray objectAtIndex:a];
			[_subjectModulesTabView addTabViewItem:b];
			b = null;
		}

		console.log("UOWSubjectView: initialisation has finished.");
	}
	return self;
}


@end
/*
switch(currPage){
			case 'SOLSMAIL':
						var tab1 = [[CPTabViewItem alloc] initWithIdentifier:@"unifiedInboxTab"];
							[tab1 setLabel:@"All Mail"];
							[tab1 setView:[_messagesInbox returnSetView]];
						var tab2 = [[CPTabViewItem alloc] initWithIdentifier:@"Notifications"];
							[tab2 setLabel:@"Notifications"];
							[tab2 setView:_informationView];
						var tab3 = [[CPTabViewItem alloc] initWithIdentifier:@"CSCI114Tab"];
							[tab3 setLabel:@"CSCI114"];
							[tab3 setView:nil];
						var tab4 = [[CPTabViewItem alloc] initWithIdentifier:@"CSCI103Tab"];
							[tab4 setLabel:@"CSCI103"];
							[tab4 setView:nil];
						var tab5 = [[CPTabViewItem alloc] initWithIdentifier:@"ISIT102Tab"];
							[tab5 setLabel:@"ISIT102"];
							[tab5 setView:nil];
						var tab6 = [[CPTabViewItem alloc] initWithIdentifier:@"STAT131Tab"];
							[tab6 setLabel:@"STAT131"];
							[tab6 setView:nil];

						var tabArray = [[CPArray alloc] initWithObjects:[tab1, tab2, tab3, tab4, tab5, tab6] count:6];
			break;
			case 'SOLSREGISTRATION':
						var tab1 = [[CPTabViewItem alloc] initWithIdentifier:@"unifiedObligationsTab"];
							[tab1 setLabel:@"All Obligations"];
							[tab1 setView:[_messagesOInbox returnSetView]];
						var tab2 = [[CPTabViewItem alloc] initWithIdentifier:@"obligationRequested"];
							[tab2 setLabel:@"Requested"];
							[tab2 setView:_informationView];
						/*var tab3 = [[CPTabViewItem alloc] initWithIdentifier:@"obligationPending"];
							[tab3 setLabel:@"Pending"];
							[tab3 setView:nil];
						var tab4 = [[CPTabViewItem alloc] initWithIdentifier:@"obligationRejected"];
							[tab4 setLabel:@"Rejected"];
							[tab4 setView:nil];
						var tab5 = [[CPTabViewItem alloc] initWithIdentifier:@"obligationAccepted"];
							[tab5 setLabel:@"Accepted"];
							[tab5 setView:nil];

						var tabArray = [[CPArray alloc] initWithObjects:[tab1, tab2, tab3] count:3]; //, tab4, tab5] count:5];
			break;
			default:
						var tab1 = [[CPTabViewItem alloc] initWithIdentifier:@"noGroup"];
							[tab1 setLabel:@" "];
							[tab1 setView:nil];
						var tabArray = [[CPArray alloc] initWithObjects:[tab1] count:1];
			break;
		}

		for(a=0; a < [tabArray count]-1;a++){
			var b = [tabArray objectAtIndex:a];
			[_tabView addTabViewItem:b];
			b = null;
		}
*/
