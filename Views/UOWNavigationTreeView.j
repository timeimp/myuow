/*
 * UOWNavigationTreeView.j
 * MyUOW
 *
 * Created by James Wilson (timeimp) on June 19, 2012.
 * Copyright 2012, Your Company All rights reserved.
 */

@implementation UOWNavigationTreeView : CPOutlineView
{
	id delegate @accessors;
    RootNodeCSS = [];
}

-(id)initWithFrame:(CGRect)aFrame{
	self = [super initWithFrame:aFrame];
	if(self){
		console.info("UOWNavigationTreeView: begun initialisation...");
		columnLabel = [[CPTableColumn alloc] initWithIdentifier:"children"],

		[self setAutoresizingMask:CPViewHeightSizable | CPViewMaxXMargin];
        [self setHeaderView:nil];
        [self setCornerView:nil];
        [self setBackgroundColor:nil];
        [self setColumnAutoresizingStyle:CPTableViewLastColumnOnlyAutoresizingStyle];
        [self addTableColumn:columnLabel];
        //[self setOutlineTableColumn:columnLabel];
        [self setTarget:self];
        [self setAllowsMultipleSelection:NO];
        [self setRowHeight:20.0];
        [self setIntercellSpacing:CPSizeMake(0.0, 5.0)];
        [self setAllowsMultipleSelection:NO];
    	[self expandItem:nil expandChildren:YES];
        //[reciever progressNowIndeterminate:YES];
	}
	return self;
}
-(BOOL)outlineView:(UOWNavigationTreeView)outlineView shouldSelectItem:(id)item{
    if([self levelForItem:item] === nil)
        return NO;
}
@end
