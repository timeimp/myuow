/*
 * AppController.j
 * MyUOW
 *
 * Created by James Wilson (timeimp) on June 19, 2012.
 * Copyright 2012, Your Company All rights reserved.
 */

@import <Foundation/CPObject.j>
@import <AppKit/CPView.j>

@implementation UOWGlobalPreferencesView : CPWindow{
	CPView topView, middleView, bottomView;
}

-(id)initWithContentRect:(CGRect)aContentRect styleMask:(unsigned int)aStyleMask{

	self = [super initWithContentRect:aContentRect styleMask:aStyleMask];

	if(self){

		var cView = [self contentView],
			cHeight = CGRectGetHeight([[self contentView] bounds]),
			cWidth = CGRectGetWidth([[self contentView] bounds]);

		var preferencesString = [[CPTextField alloc] initWithFrame:CGRectMakeZero()];
		[preferencesString setStringValue: @"Preferences"];

		var preferencesInformation = [[CPTextField alloc] initWithFrame:CGRectMakeZero()];
		[preferencesInformatisson setStringValue: @"Change MyUOW preferences to your liking. Some changes may require you to reload MyUOW."];

		topView = [[CPView alloc] initWithFrame:CGRectMake(0, 0, cWidth, 50)];
		[topView setBackgroundColor:[CPColor grayColor]];


		middleView = [[CPView alloc] initWithFrame:CGRectMake(0, 50, cWidth, cHeight-40)];
		[middleView setBackgroundColor:[CPColor grayColor]];

		bottomView = [[CPView alloc] initWithFrame:CGRectMake(0, cHeight - (cHeight-40), cWidth, 40)];
		[bottomView setBackgroundColor:[CPColor lightGrayColor]];

		[cView addSubview:topView];
		[cView addSubview:middleView];
		[cView addSubview:bottomView];

	}

	return self;
}


@end
