/*
 * AppController.j
 * MyUOW
 *
 * Created by James Wilson (timeimp) on June 19, 2012.
 * Copyright 2012, timeimp inc. All rights reserved.
 */

@import <Foundation/Foundation.j>
@import <AppKit/CPView.j>

@implementation UOWLoginWindow : CPWindow {
	CPTextField	productTitle, productVersion, usernameField, passwordField, tempString;
	CPButton	loginButton;
	CPImage		backgroundImage, loginImage, loadingImage;
	CPImageView	backgroundImageView, loginImageView, loadingImageView;
	CPView		loginView;
	CPView 		loadingImageCPView;
	id			delegate @accessors;
}


- (id) initWithContentRect:(CGRect)aContentRect styleMask:(unsigned int)aStyleMask{
	self = [super initWithContentRect:aContentRect styleMask:aStyleMask];
 
	if(self){

		console.info('LoginWindowView: initialising...');
		console.info('LoginWindowView: now accessing the background image...');

		var mainFrame = CGRectMake(0,0,CGRectGetWidth([[self contentView] bounds]),CGRectGetHeight([[self contentView] bounds]));

		var loginView = [[CPView alloc] initWithFrame:CGRectMake(0,CGRectGetHeight([[self contentView] bounds])-50,
		    														CGRectGetWidth([[self contentView] bounds]),
		    														50)],
			backgroundImage = [[CPImage alloc] initWithContentsOfFile:@"Resources/Backgrounds/loginWindow-OperaHouse.jpg"],
		    backgroundImageView = [[CPImageView alloc] initWithFrame:mainFrame],
		    loginImage = [[CPImage alloc] initWithContentsOfFile:@"Resources/Backgrounds/UOWEmblem.png"],
		    loginImageView = [[CPImageView alloc] initWithFrame:CGRectMake(5,2.5, 153, 45)];

		[loginView setBackgroundColor:[CPColor whiteColor]];

		[backgroundImageView setImage:backgroundImage];
		[backgroundImageView setHidden:NO];
		[backgroundImageView setAutoresizingMask: CPViewMinXMargin | CPViewMaxXMargin | CPViewMinYMargin | CPViewMaxYMargin];
		[backgroundImageView setImageScaling:CPScaleToFit];
		[backgroundImageView setImageAlignment:CPImageAlignCenter];

		[loginImageView setImage:loginImage];
		[loginImageView setHidden:NO];
		[loginImageView setImageScaling:CPScaleNone];
		[loginImageView setImageAlignment:CPImageAlignCenter];

		[loginView addSubview:loginImageView];

		var tempString = [[CPTextField alloc] initWithFrame:CGRectMakeZero()];
		[tempString setFont:[CPFont systemFontOfSize:14.0]];
		[tempString setFrameOrigin:CGPointMake(165, 24)];
		[tempString setStringValue:@"To continue you need to login."];
		[tempString sizeToFit];
		[loginView addSubview:tempString];

		usernameField = [[CPTextField alloc] initWithFrame:CGRectMake(CGRectGetWidth([loginView bounds])/2-155, 6, 150, 34)];
		[usernameField setFont:[CPFont systemFontOfSize:16.0]];
		[usernameField setBordered:YES];
		[usernameField setBezeled:YES];
		[usernameField setDrawsBackground:NO];
		[usernameField setEditable:YES];
		[usernameField setPlaceholderString:@"Username"];
		[usernameField setTextColor:[CPColor blackColor]];
		[loginView addSubview:usernameField];

		passwordField = [[CPTextField alloc] initWithFrame:CGRectMake(CGRectGetWidth([loginView bounds])/2+5, 6, 150, 34)];
		[passwordField setFont:[CPFont systemFontOfSize:16.0]];
		[passwordField setBordered:YES];
		[passwordField setBezeled:YES];
		[passwordField setDrawsBackground:NO];
		[passwordField setEditable:YES];
		[passwordField setSecure:YES];
		[passwordField setPlaceholderString:@"Password"];
		[passwordField setTextColor:[CPColor blackColor]];
		[loginView addSubview:passwordField];

		var loginButton = [[CPButton alloc] initWithFrame:CGRectMake(CGRectGetWidth([loginView bounds])/2+165, 11, 100, 24)];
		[loginButton setTitle:@"Login"];
		[loginButton setTarget:self];
		[loginButton setAction:@selector(performLogin:)];
		[loginButton setKeyEquivalent:CPCarriageReturnCharacter];

		var loginXPos = CGRectGetWidth([loginView bounds]) - (CGRectGetWidth([loginView bounds]) - (CGRectGetWidth([tempString bounds]) + 165));
		var loadingImageCPView = [[CPView alloc] initWithFrame:CGRectMake(	loginXPos,
																			CGRectGetHeight([[self contentView] bounds])-50,
																			loginXPos+CGRectGetWidth([loginButton bounds])+165,50)],
			loadingImage = [[CPImage alloc] initWithContentsOfFile:@"Resources/Loaders/processing.gif"],
			loadingImageView = [[CPImageView alloc] initWithFrame:CGRectMake(CGRectGetWidth([loadingImageCPView bounds])/2-110, 15, 220, 20)];
		[loadingImageView setImage:loadingImage];
		[loadingImageView setImageAlignment:CPImageAlignCenter];

		[loadingImageCPView setHidden:YES];
		[loadingImageCPView setBackgroundColor:[CPColor whiteColor]];
		[loadingImageCPView addSubview:loadingImageView];

		[loginView addSubview:loginButton];

		console.info('LoginWindowView: image has been loaded into memory...');

		[loginView setAutoresizingMask: CPViewMinXMargin | CPViewMaxXMargin | CPViewMinYMargin | CPViewMaxYMargin];
		[[self contentView] addSubview:loginView positioned:CPWindowAbove relativeTo:backgroundImageView];
		[[self contentView] addSubview:backgroundImageView positioned:CPWindowBelow relativeTo:loginView];
		[[self contentView] addSubview:loadingImageCPView positioned:CPWindowAbove relativeTo:loginView];

		[self setProductTitle:@"MyUOW" withVersion:0.1];
   		[[CPNotificationCenter defaultCenter] addObserver:self selector:@selector(redoLogin:) name:@"UOWLoginWindow_RedrawLoginField" object:nil];
		console.info('LoginWindowView: initialisation finished.');
	}
	return self;
}
-(void)setProductTitle:(CPString)aTitle withVersion:(float)aVersion{
	var	productTitle = [[CPTextField alloc] initWithFrame:CGRectMakeZero()];
		productVersion = [[CPTextField alloc] initWithFrame:CGRectMakeZero()];

	[productTitle setStringValue:aTitle];
	[productVersion setStringValue:aVersion];

	[productTitle setFrameOrigin:CGPointMake(165, 0)];
	[productTitle setFont:[CPFont boldSystemFontOfSize:22.0]];
	[productTitle setTextColor:[CPColor grayColor]];

	[productTitle sizeToFit];
	[loginView addSubview:productTitle];
	//[loginView addSubview:productVersion];
}
-(void)performLogin:(id)sender{
	[loadingImageCPView setHidden:NO];
	console.info("performLogin called");
	[delegate beginUserAuthenticationWithUser:[usernameField stringValue] andPassword:[passwordField stringValue]];
}
-(void)redoLogin:(id)sender{
	[passwordField setObjectValue:nil];
	[loadingImageCPView setHidden:YES];
}

@end
