/*
 * AppController.j
 * MyUOW
 *
 * Created by James Wilson (timeimp) on June 19, 2012.
 * Copyright 2012, Your Company All rights reserved.
 */
@import <Foundation/CPObject.j>
@import <AppKit/CPView.j>

@import "../Controllers/UOWNavigationTreeController.j"
@import "../Views/UOWNavigationTreeView.j"
@import "../Views/UOWSubjectView.j"
@import "../Views/UOWActivityView.j"

@implementation UOWMainApplicationWindow : CPWindow{
	CPView topSide, leftSide, rightSide, splitView;
	UOWNavigationTreeController navigationController;
	UOWNavigationTreeView		navigationView;
	UOWActivityView				globalActivityView;
}

-(id)initWithContentRect:(CGRect)aContentRect styleMask:(unsigned int)aStyleMask{
	self = [super initWithContentRect:aContentRect styleMask:aStyleMask];
	if(self){
		console.info("UOWMainApplicationWindow has been initialised...");

		/* Set up ContentView variables that will be used within this scope. */
		var cView = [self contentView],
			cViewHeight = CGRectGetHeight([[self contentView] bounds]),
			cViewWidth = CGRectGetWidth([[self contentView] bounds]);

		topSide = [[CPView alloc] initWithFrame:CGRectMake(0,0,CGRectGetWidth([[self contentView] bounds]), 50)];

		/* Create our splitView */
		splitView = [[CPSplitView alloc] initWithFrame:CGRectMake(0, 50, cViewWidth, cViewHeight-50)];
		[splitView setDelegate:self];
		[splitView setVertical:YES];
		[splitView setAutoresizingMask:CPViewWidthSizable | CPViewHeightSizable ];

		//[horizontalSplitter setIsPaneSplitter:NO];

		/* Create the left and right splitView subViews */
		leftView = [[CPView alloc] initWithFrame:CGRectMake(0, 0, 250, CGRectGetHeight([splitView bounds]))];
		[leftView setBackgroundColor: [CPColor colorWithHexString:@"E4EBF7"]];
		[leftView setAutoresizingMask:CPViewWidthSizable | CPViewHeightSizable];

		rightView = [[CPView alloc] initWithFrame:CGRectMake(250, 50, CGRectGetWidth([splitView bounds]) - 250, CGRectGetHeight([splitView bounds]))];
		[rightView setAutoresizingMask:CPViewWidthSizable | CPViewHeightSizable];

		/* Create and load out new UOWNavigationTree stuff. Create the view, then the controller. */
		var navigationView = [[UOWNavigationTreeView alloc] initWithFrame:CGRectMake(0.0, 0.0, CGRectGetWidth([leftView frame]), CGRectGetHeight([leftView frame]))];
		var navigationController = [[UOWNavigationTreeController alloc] init];
		[navigationController setView:navigationView];

		/* To allow scrolling, we must create a scroll view.
			So lets to that now... */

		var scrollView = [[CPScrollView alloc] initWithFrame:CGRectMake(0.0, 0.0, CGRectGetWidth([leftView frame]), CGRectGetHeight([leftView frame])-80)];
		[scrollView setDelegate:self];
		[scrollView setDocumentView:navigationView];
		[scrollView setAutoresizingMask:CPViewWidthSizable | CPViewHeightSizable];
		[scrollView setAutohidesScrollers:YES];
		[scrollView setHasHorizontalScroller:NO];
		[leftView addSubview:scrollView];

		/* Set our background banner colour as white */
		[topSide setBackgroundColor:[CPColor whiteColor]];

		/* Create and populate the Product Title, current user and their user ID */

		var productTitle = [[CPTextField alloc] initWithFrame:CGRectMakeZero()];
			productUser = [[CPTextField alloc] initWithFrame:CGRectMakeZero()];
			productUserID = [[CPTextField alloc] initWithFrame:CGRectMakeZero()];

		[productTitle setStringValue:@"MyUOW"];
		[productUser setStringValue:@"James Wilson"];
		[productUserID setStringValue:@"(4258447)"];

		[productTitle setFont:[CPFont boldSystemFontOfSize:32.0]];
		[productUser setFont:[CPFont systemFontOfSize:16.0]];
		[productUserID setFont:[CPFont systemFontOfSize:16.0]];

		[productTitle setTextColor:[CPColor grayColor]];

		[productTitle sizeToFit];
		[productUser sizeToFit];
		[productUserID sizeToFit];

		[productTitle setFrameOrigin:CGPointMake(165, 0)];

		/* As our login window is shown first we:
			1) Create the and load the image and imageView
			2) Create a login button.
		*/

		var loginImage = [[CPImage alloc] initWithContentsOfFile:@"Resources/Backgrounds/UOWEmblem.png"],
		    loginImageView = [[CPImageView alloc] initWithFrame:CGRectMake(5,2.5, 153, 45)];

		[loginImageView setImage:loginImage];
		[loginImageView setHidden:NO];
		[loginImageView setImageScaling:CPScaleNone];
		[loginImageView setImageAlignment:CPImageAlignCenter];

		[topSide addSubview:loginImageView];

		var logoutButton = [[CPButton alloc] initWithFrame:CGRectMakeZero()];
		[logoutButton setTitle:@"Logout"];
		[logoutButton sizeToFit];
		[logoutButton setFrameOrigin:CGPointMake(CGRectGetWidth([topSide bounds])-CGRectGetWidth([logoutButton bounds])-7, CGRectGetHeight([topSide bounds])/2 - CGRectGetHeight([logoutButton bounds])/2)];

		var topSidePosX = CGRectGetWidth([topSide bounds]) - CGRectGetWidth([logoutButton bounds]) - 7;
		var topSidePosY = CGRectGetHeight([topSide bounds]) / 2 -CGRectGetHeight([productUser frame]) / 2;

		/* Create our GLOBAL activityView */

        globalActivityView = [[UOWActivityView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight([scrollView frame]), CGRectGetWidth([leftView frame]), 80)];
        [leftView addSubview:globalActivityView positioned:CPWindowAbove relativeTo:scrollView];

        var uploader = [[UOWActivityBlock alloc] initWithFrame:CGRectMake(0.0, 24+(0 * 36), CGRectGetWidth([globalActivityView frame]), 36)];
        [uploader setTitle:@"Uploading files..."];
        [globalActivityView addSubview:uploader];


		/* Position our elements! */
		[productUser setFrameOrigin:CGPointMake(topSidePosX - CGRectGetWidth([productUser frame]) - CGRectGetWidth([productUserID bounds]) - 5 , topSidePosY)];
		[productUserID setFrameOrigin:CGPointMake(topSidePosX - CGRectGetWidth([productUserID frame]) - 5, topSidePosY)];

		/* Add our subviews */
		[topSide addSubview:productTitle];
		[topSide addSubview:productUser];
		[topSide addSubview:productUserID];
		[topSide addSubview:logoutButton];
	    [cView addSubview:topSide];

	    [splitView addSubview:leftView];
		[splitView addSubview:rightView];
		[cView addSubview:splitView];

	}
	return self;
}

- (float)splitView:(CPSplitView)aSplitView constrainMinCoordinate:(float)proposedMin ofSubviewAt:(int)subviewIndex{
	return proposedMin + 250;
}

-(void)startSubjectController:(CPNotification)aNotification{
	console.log("Starting startSubjectController...");

}

@end
