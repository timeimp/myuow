/*
	UOWActivityView.j
*/

@import <Foundation/Foundation.j>
@import <AppKit/CPOutlineView.j>
@import <Foundation/CPURLConnection.j>
/*
UOWNavigationTreeViewReload                   = @"TNArchipelRosterOutlineViewReload";
UOWNavigationTreeViewDidShowNotification      = @"TNArchipelPropertiesViewDidShowNotification";
UOWNavigationTreeViewDeselectAll              = @"TNArchipelRosterOutlineDeselectAll";
UOWNavigationTreeViewSelectItemNotification   = @"TNArchipelRosterOutlineViewSelectItemNotification";
*/

@implementation UOWActivityBlock : CPView
{
        CPProgressIndicator _anIndicator @accessors(property=indicator);
        CPTextField _title;
        CPTextField _timeRemaining;
        CPNumber currentBlockCount;

}


-(id)initWithFrame:(CGRect)aFrame{
        self = [super initWithFrame:aFrame];
        currentBlockCount = [[CPNumber alloc] initWithInt:0];
        if(self){
                [self setAutoresizingMask:CPViewWidthSizable | CPViewHeightSizable];
                var width = CGRectGetMaxX(aFrame) - 20;

                anIndicator = [[CPProgressIndicator alloc] initWithFrame:CGRectMake(10.0,0.0,width, 16)];
                [anIndicator setAutoresizingMask:CPViewWidthSizable];
                [anIndicator startAnimation:self];

                _title = [[CPTextField alloc] initWithFrame:CGRectMake(10.0,20.0,width, 16.0)];
                //_timeRemaining = [[CPTextField alloc] initWithFrame:CGRectMakeZero()];
                [_title setFont:[CPFont systemFontOfSize:10.0]];
                [_title setTextFieldBackgroundColor:[CPColor lightGrayColor]];

                [self addSubview:anIndicator];
                [self addSubview:_title];
        }
        currentBlockCount++;
        return self;
}

-(void)setMin:(int)min withMax:(int)max{
        [anIndicator setMinValue:min];
        [anIndicator setMaxValue:max];
}

-(void)setTitle:(CPString)aTitle{
        [_title setObjectValue:aTitle];
        console.log(aTitle);
}

-(void)updateProgress:(id)sender {
        if([sender isKindOfClass:[CPNotification class]])
                var sender = [sender object];

        var incrementAmount = MAX([self.indicator doubleValue], sender);
        incrementAmount = incrementAmount - [self.indicator doubleValue];
        [self.indicator setDoubleValue:incrementAmount];
        console.log("Indicator:",self.indicator);
}

-(void)progressNowIndeterminate:(id)sender{
        var newCurrent = sender;
        if([sender isKindOfClass:[CPNotification class]])
                var newCurrent = [sender object];

        [anIndicator setIndeterminate:newCurrent];
        console.warn([anIndicator isIndeterminate]);
}
@end

@implementation UOWActivityView : CPView
{
}

-(id)initWithFrame:(CGRect)aFrame{
        self = [super initWithFrame:aFrame];
        if(self){
                [self setBackgroundColor:[CPColor colorWithHexString:@"DAE1ED"]];
                var textField = [[CPTextField alloc] initWithFrame:CGRectMakeZero()];
                [textField setObjectValue:@"ACTIVITY"];
                [textField setAutoresizingMask:CPViewMinXMargin | CPViewMaxXMargin];
                [textField sizeToFit];
                [self addSubview:textField];

                [self setAutoresizingMask:CPViewWidthSizable | CPViewHeightSizable];
                [self setAutoresizesSubviews:YES];

                [textField setFrameOrigin:CGPointMake(CGRectGetMaxX([self frame])/2 - CGRectGetWidth([textField frame])/2,5)];
                [[CPNotificationCenter defaultCenter] addObserver:self selector:@selector(createNewActivityWithTitle:) name:@"UOWActivityBlockCreate" object:self];
        }
        return self;
}

@end
