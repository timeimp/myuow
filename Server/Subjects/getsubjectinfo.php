<?php
	
	$information = array("TabItems"=>array(
			array("displayName"=>"Subject Information"),
			array("displayName"=>"Lecture Slides"),
			array("displayName"=>"Tutorial Notes"),
			array("displayName"=>"Assignments"),
			array("displayName"=>"Direct Messages")
				));

	$subject = array("UOWSubjectPayload" => $information);
	array_push($subject, array("status"=>"SUCCESS"));
	header("Content-Type:application/json");

	echo json_encode($subject);
?>