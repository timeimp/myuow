<?php
/******************
Get user subject. Return the items as JSON.
******************/
error_reporting('E_ALL');
require_once("../DB/mysql.conf.php");

$user = $_GET['user'];
$stuid = $_GET['stuid'];
header("Content-Type: application/json");
# Prepare the Query

try{
	$statement = $UOW_DBH->prepare("SELECT * FROM enrolled_subjects WHERE student_username = :user AND student_id = :stuid");
	$statement->bindParam(':user', $user);
	$statement->bindParam(':stuid', $stuid);
	$statement->execute();
	$row = $statement->fetchAll();
	$i = 1;
	$children = array();
	foreach($row as $result){
		$r = array("s_code"=>$result['enrollment_code'],"s_level"=>$result['enrollment_level'],"s_name"=>($result['enrollment_code'].$result['enrollment_level']));
		array_push($children, $r);
	}
	$subject = array("UOWNavigationPayload" => array("parentNode"=>array("name" => "Subjects","childNodes" => $children)));
	echo json_encode($subject);
	$UOW_DBH = NULL;
} catch(PDOException $e) {  
 	echo $e->getMessage();  
}  
// Use fetchAll() if you want all results, or just iterate over the statement, since it implements Iterator

?>