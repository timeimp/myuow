<?php
/******************
Get user subject. Return the items as JSON.
******************/
error_reporting('E_ALL');
require_once("../DB/mysql.conf.php");

$user = $_GET['user'];
$stuid = $_GET['stuid'];
header("Content-Type: application/json");
# Prepare the Query

try{
	$statement = $UOW_DBH->prepare("SELECT * FROM enrolled_subjects WHERE student_username = :user AND student_id = :stuid");
	$statement->bindParam(':user', $user);
	$statement->bindParam(':stuid', $stuid);
	$statement->execute();
	$row = $statement->fetchAll();
	$i = 1;
	$children = array();
	$parents = array();
	foreach($row as $result){
		$r = array("s_code"=>$result['enrollment_code'],"s_level"=>$result['enrollment_level'],"displayName"=>($result['enrollment_code'].$result['enrollment_level']));
		array_push($children, $r);
	}
	$viewMore = array("s_code" => "previous", "displayName"=>"Previous Subjects");
	array_push($children, $viewMore);
	$parentNode = array("parentNode"=>array("name" => "Subjects","childNodes" => $children));
	$pNode = array("parentNode"=> array("name"=>"Timetables", "childNodes" => array(array("displayName"=>"Session Timetable"),array("displayName"=>"Exam Timetable"))));
	$pNode2 = array("parentNode"=> 
				array("name"=>"Personal Details", 
				"childNodes" => array(
					array("displayName"=>"Personal and Emergency Details"),
					array("displayName"=>"Address and Contact Info"),
					array("displayName"=>"Emergency Contacts"),					
					)
				)
			);
	$pNode3 = array("parentNode"=> 
				array("name"=>"Enrolments and Variations", 
				"childNodes" => array(
					array("displayName"=>"Tutorial Enrolment"),
					array("displayName"=>"Subject Enrolment"),
					array("displayName"=>"Assignment Results"),
					array("displayName"=>"Enrolment Record (Final Grades)"),
					array("displayName"=>"Degree Maintenance"),
					array("displayName"=>"Academic Consideration")
					)
				)
			);
	$pNode4 = array("parentNode"=> 
				array("name"=>"Fees and Payments", 
				"childNodes" => array(
					array("displayName"=>"Internet Payment"),
					array("displayName"=>"HELP Loan Option"),
					array("displayName"=>"Fee Statement"),
					array("displayName"=>"Commonwealth Assistance Notice"),
					array("displayName"=>"UniCentre and URAC Membership")
					)
				)
			);
	$pNode5 = array("parentNode"=> 
				array("name"=>"Help and Other Information", 
				"childNodes" => array(
					array("displayName"=>"UOW IT Policies"),
					array("displayName"=>"UOW Events"),
					array("displayName"=>"SOLSHelp"),
					array("displayName"=>"Contact Student Central")
					)
				)
			);
	array_push($parents, $parentNode);
	array_push($parents, $pNode);
	array_push($parents, $pNode2);
	array_push($parents, $pNode3);
	array_push($parents, $pNode4);
	array_push($parents, $pNode5);
	$subject = array("UOWNavigationPayload" => $parents); 
	echo json_encode($subject);
	$UOW_DBH = NULL;
} catch(PDOException $e) {  
 	echo $e->getMessage();  
}  
// Use fetchAll() if you want all results, or just iterate over the statement, since it implements Iterator

?>