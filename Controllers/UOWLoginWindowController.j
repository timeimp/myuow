/*
 * AppController.j
 * MyUOW
 *
 * Created by James Wilson (timeimp) on June 19, 2012.
 * Copyright 2012, timeimp inc. All rights reserved.
 */

@import <Foundation/CPObject.j>
@import "../Models/UOWAuthenticationModel.j"
@import "../Views/UOWLoginWindow.j"


 @implementation UOWLoginWindowController : CPWindowController{
 	UOWAuthenticationObject authObject;
 }

 -(id)initWithWindow:(CPWindow)aWindow {
 		self = [super initWithWindow:aWindow];
 		if(self){
 			console.info('LoginWindowController initialised.');
 			[aWindow setDelegate:self];
 			authObject = [[UOWAuthenticationObject alloc] init];
 			[authObject setDelegate:self];
 		}
 		return self;
 }

 -(void)showLoginWindow{
 		console.info('LoginWindowController showing window...');
 }

 -(BOOL)beginUserAuthenticationWithUser:(CPString)aUser andPassword:(CPString)aPassword{
 	console.info("beginUserAuthenticationWithUser has been called. UOWAuthenticationObject now calling...");
 	[authObject authenticateWithUser:aUser andPassword:aPassword];
 }

-(void)didFailAuthenticationWithError:(CPString)anError{
	var alert = [CPAlert alertWithMessageText:anError defaultButton:@"OK" alternateButton:nil otherButton:nil informativeTextWithFormat:@"An invalid username or password was provided"];
	[alert runModal];
	[[CPNotificationCenter defaultCenter] postNotificationName:@"UOWLoginWindow_RedrawLoginField" object:nil];
}

-(void)didSucceedAuthenticating{
	[[CPNotificationCenter defaultCenter] postNotificationName:@"UOWBeginLogin" object:nil];
}

 @end
