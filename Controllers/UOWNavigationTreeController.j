/*
 * AppController.j
 * MyUOW
 *
 * Created by James Wilson (timeimp) on June 19, 2012.
 * Copyright 2012, Your Company All rights reserved.
 */

@import <Foundation/Foundation.j>
@import <AppKit/CPOutlineView.j>
@import <Foundation/CPURLConnection.j>
@import "../Views/UOWNavigationTreeView.j"

@implementation UOWNavigationTreeController : CPObject{
 			UOWNavigationTreeView _theView;
 			CPURLConnection getPagesURL;
 			id delegate @accessors();
 			CPArray TreeItems;
 			CPDictionary treeList;
 }

-(id)init{
	self = [super init];
	if(self){
			console.warn("UOWNavigationTreeController: begun initialisation...");
			getPagesURL = [[CPURLConnection alloc] initWithRequest:[CPURLRequest requestWithURL:@"Server/UserInformation/getEnrolledPages.php?user=jw192&stuid=4258447"] delegate:self];

			}
	return self;
}
-(void)setView:(UOWNavigationTreeView)aView{
	_theView = aView;
}


-(void)connection:(CPURLConnection)connection didReceiveData:(CPString)data{
			//subjectURL = [[CPURLConnection alloc] initWithRequest:[CPURLRequest requestWithURL:@"Server/Subjects/getusersubjects.php?user=jw192&stuid=4258447"] delegate:self];
			//timetableURL = [[CPURLConnection alloc] initWithRequest:[CPURLRequest requestWithURL:@"Server/Timetables/getusertimetables.php?user=jw192&stuid=4258447"] delegate:self];
			//enrollmentsAndResultsURL = [[CPURLConnection alloc] initWithRequest:[CPURLRequest requestWithURL:@"Server/EnrollmentsAndResults/getuserenrolmentsandresults.php?user=jw192&stuid=4258447"] delegate:self];
			//feeURL = [[CPURLConnection alloc] initWithRequest:[CPURLRequest requestWithURL:@"Server/Fees/getuserfees.php?user=jw192&stuid=4258447"] delegate:self];

	var		theDataJSON = [data objectFromJSON],
		 	theData = theDataJSON.UOWNavigationPayload,
			TreeItems = [CPArray array];

	for(i = 0; i < theData.length; i++){
		var treeList = [CPDictionary dictionary],
			menuArray = [CPArray array];
		[treeList setObject:theData[i].parentNode.name forKey:@"parent"];
		[[CPNotificationCenter defaultCenter] postNotificationName:@"UOWPageAdd" object:theData[i].parentNode.name];
		for(j = 0; j < theData[i].parentNode.childNodes.length; j++){
			[menuArray addObject:theData[i].parentNode.childNodes[j].displayName];
		}
		[treeList setObject:menuArray forKey:@"children"];
		[TreeItems addObject:treeList];
		menuArray = [];
	}
	[_theView setDataSource:self];
	[_theView setDelegate:self];

	[[CPNotificationCenter defaultCenter] addObserver:self
               selector:@selector(outlineViewSelectionDidChange:)
                   name:@"CPOutlineViewSelectionDidChangeNotification"
                 object:_theView];
	[[CPNotificationCenter defaultCenter] addObserver:self
               selector:@selector(outlineViewSelectionIsChanging:)
                   name:@"CPOutlineViewSelectionIsChangingNotification"
                 object:_theView];
}

-(id)outlineView:(UOWNavigationTreeView)outlineView child:(int)index ofItem:(id)item{
    if (item === nil){
    	itemArray = TreeItems;
    	if([itemArray isKindOfClass:[CPArray class]]){
    		var item = itemArray[index];
    	}
    	return item;
    }
/*
  RootNodeCSS = [
        [@"text-transform", @"uppercase", CPThemeStateDefault],
        [@"color",          [CPColor colorWithHexString:@"336699"], CPThemeStateDefault]
    ];
*/
    if([item isKindOfClass:[CPDictionary class]]){
    	var item = [[item objectForKey:@"children"] objectAtIndex:index];
    	return item;
    }

}
-(BOOL)outlineView:(UOWNavigationTreeView)outlineView isItemExpandable:(id)item{
 	if ([item isKindOfClass:[CPDictionary class]]) {
        return YES;
    }else {
        return NO;
    }
}

-(int)outlineView:(UOWNavigationTreeView)outlineView numberOfChildrenOfItem:(id)item {
	if (item === nil){
		item = TreeItems;
		return [TreeItems count];
	} else {
		var returnItem = nil;
		if([item isKindOfClass:[CPDictionary class]]){
			returnItem = [[item objectForKey:@"children"] count];
		} else {
			returnItem = [item count];
		}
		return returnItem;
	}
}

-(id)outlineView:(UOWNavigationTreeView)outlineView objectValueForTableColumn:(CPTableColumn)tableColumn byItem:(id)item {
  if ([item isKindOfClass:[CPDictionary class]]) {
            return [item objectForKey:@"parent"];
  } else {

  		return item;
  }
}

-(void)outlineViewSelectionIsChanging:(CPNotification)notification{
	//console.log("Changing...");
	//console.log([_theView clickedRow]);
	//[_theView outlineView:_theView shouldSelectItem:[_theView itemAtRow:[_theView clickedRow]]];
}
-(void)outlineViewSelectionDidChange:(CPNotification)notification{
  	var theSelectedRow 	= [_theView itemAtRow:[_theView selectedRow]];
  	var theParent		= [[_theView parentForItem:theSelectedRow] objectForKey:@"parent"];
	if([_theView levelForItem: theSelectedRow] > 0){
		var pageToLoad 	= [[CPDictionary alloc] initWithObjectsAndKeys: theSelectedRow, @"page", theParent, @"parent"];
		[[CPNotificationCenter defaultCenter] postNotificationName:@"UOWPageLoadBegin" object:pageToLoad];
		console.log("Loading new page...",[_theView levelForItem: theSelectedRow],theSelectedRow,theParent);
	}
}

// - (BOOL)selectionShouldChangeInOutlineView:(CPOutlineView)outlineView;
@end
