/*
 * AppController.j
 * MyUOW
 *
 * Created by James Wilson (timeimp) on July 20, 2012.
 * Copyright 2012, Your Company All rights reserved.
 */
@import <AppKit/CPObject.j>
@import "Views/UOWSubjectView.j"

@implementation UOWSubjectController : CPObject{
	UOWSubjectView _theView;
	CPURLRequest _subjectRequest;
	CPURLConnection _subjectURL;
}

-(id)init:(id)sender forFrame:(CGRect)aFrame{
	self = [super init:sender];

	if(self){
		console.log("UOWSubjectController: beginning initialisation...");
		_theView = [[UOWSubjectView alloc] initWithFrame:aFrame];

		_subjectRequest = [[CPURLRequest alloc] initWithURL:@"Server/Subjects/getusersubjects.php"];

		_subjectURL = [[CPURLConnection alloc] initWithRequest:_subjectRequest delegate:self];

		[[CPNotificationCenter defaultCenter] addObserver:self selector:@selector(updateSubjectInformation:) name:@"loadSubject" object:nil];

	}

	return self;
}

-(void)connection:(CPURLConnection)_subjectURL didFailWithError:(id)error{
	console.warn("UOWSubjectController: an error occurred with details: "+error);
	[delegate didFailSubjectLoadWithError:@"Unable to retrieve subject information"];
}
-(void)connection:(CPURLConnection)_subjectURL didReceiveData:(CPString)data{
	var theData = [data objectFromJSON];
	console.info("UOWSubjectController: finished loading _subjectURL.")
	console.log(theData);
	var payloadData = theData.UOWSubjectPayload;
	if(theData.status == "SUCCESS"){
		alert("SUCCESS");
	} else {
		//[delegate didFailAuthenticationWithError:theData.errorMessage];
		alert("FAIL");
	}
}

-(void)setSubjectTitle:(CPString)titleString withDescription:(CPString)descString{
	if(titleString !== nil)
		_theView.subjectTitle = titleString;

	if(descString !== nil)
		_theView.subjectDescription;
}


@end
