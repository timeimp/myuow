/*
 * AppController.j
 * MyUOW
 *
 * Created by James Wilson (timeimp) on June 19, 2012.
 * Copyright 2012, timeimp inc. All rights reserved.
 */

@import <Foundation/CPObject.j>
@import <Foundation/CPURLRequest.j>
@import <Foundation/CPURLConnection.j>

@implementation UOWAuthenticationObject : CPObject{
	CPURLConnection authConnection;
	CPURLRequest	authRequest;
	id delegate @accessors;
}
-(id)init{
	self = [super init];
	if(self){
			console.info("New UOWAuthenticationObject created");
	}
	return self;
}


-(BOOL)authenticateWithUser:(CPString)aUser andPassword:(CPString)aPassword{
	console.log("UOWAuthenticationObject: authenticateWithUser:andPassword has been called...");
	authRequest = [[CPURLRequest alloc] initWithURL:@"Server/Authentication/initSession.php"];
	[authRequest setHTTPMethod:@"POST"];
	var payloadJSON = {"user" : aUser, "pass" : aPassword };
	var content = [CPString JSONFromObject:payloadJSON];
	[authRequest setHTTPBody:content];
	console.info("UOWAuthenticationObject: connecting to server...");
	authServer = [[CPURLConnection alloc] initWithRequest:authRequest delegate:self];
}

-(void)connection:(CPURLConnection)authServer didFailWithError:(id)error{
	console.warn("UOWAuthenticationObject: an error occurred with details: "+error);
	[delegate didFailAuthenticationWithError:@"Server Failed to Authenticate"];
}
-(void)connection:(CPURLConnection)authServer didReceiveData:(CPString)data{
	var theData = [data objectFromJSON];
	console.info("UOWAuthenticationObject: finished loading initSession URL.")
	console.log(theData);
	if(theData.status == "SUCCESS"){
		[delegate didSucceedAuthenticating];
	} else {
		[delegate didFailAuthenticationWithError:theData.errorMessage];
	}
}
@end
