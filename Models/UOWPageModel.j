/*
	UOWPageModel
*/
@import <Foundation/CPObject.j>
@import <Foundation/CPURLRequest.j>
@import <Foundation/CPURLConnection.j>

@implementation UOWPageModel : CPObject{
	CPNumber	pageCount;
	CPArray		pageArray;
}

+(void)initialize{
	pageCount = 0;
	pageArray = [[CPArray alloc] init];

	[[CPNotificationCenter defaultCenter] addObserver:self selector:@selector(processPageLoad:) name:@"UOWPageLoadBegin" object:nil];
	[[CPNotificationCenter defaultCenter] addObserver:self selector:@selector(addPage:) name:@"UOWPageAdd" object:nil];

}

-(void)addPage:(id)aPage{
	[pageArray arrayByAddingObject: aPage];
}

-(void)removePage:(id)aPage{
	[pageArray setValue:nil forKey:aPage];
}

-(void)processPageLoad:(CPNotification)aNotification{
	var sPageToLoad = [[aNotification object] objectForKey:@"parent"];
	console.log("Called:",sPageToLoad);
	/* Process and notify the correct Controllers */
	for(i=0;i<pageCount;i++){
		if(sPageToLoad == [pageArray objectAtIndex:i]){
			var noticeName = "UOW"+sPageToLoad+"ControllerBeginPageLoad";
			[[CPNotificationCenter defaultCenter] postNotificationName: noticeName object:[aNotification object]];
		}
	}
}
