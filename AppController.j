/*
 * AppController.j
 * MyUOW
 *
 * Created by James Wilson (timeimp) on June 19, 2012.
 * Copyright 2012, Your Company All rights reserved.
 */

@import <Foundation/CPObject.j>
@import "Controllers/UOWLoginWindowController.j"
@import "Views/UOWLoginWindow.j"
@import "Views/UOWMainApplicationWindow.j"

@implementation AppController : CPObject
{
    UOWLoginWindowController theLoginWindowController;
    UOWLoginWindow theLoginWindow;
    UOWMainApplicationWindow theMainApplicationWindow;
    CPNotificationCenter theNotificationCentre;
    CPCookie theAuthCookie;
    CPBox CPAlertShadow;
}

- (void)applicationDidFinishLaunching:(CPNotification)aNotification
{
    /*var theWindow = [[CPWindow alloc] initWithContentRect:CGRectMakeZero() styleMask:CPBorderlessBridgeWindowMask],
        contentView = [theWindow contentView];
*/
    var theLoginWindow = [[UOWLoginWindow alloc] initWithContentRect:CGRectMakeZero() styleMask:CPBorderlessBridgeWindowMask],
        theLoginWindowContentView = [theLoginWindow contentView],
        theLoginWindowController = [[UOWLoginWindowController alloc] initWithWindow:theLoginWindow];

    theNotificationCentre = [CPNotificationCenter defaultCenter];
    [[CPNotificationCenter defaultCenter] addObserver:self selector:@selector(initialiseMainApplicationWindow:) name:@"UOWBeginLogin" object:nil];
    [[CPNotificationCenter defaultCenter] addObserver:self selector:@selector(showAlertShadow:) name:@"willShowCPAlertShadow" object:nil];
    [[CPNotificationCenter defaultCenter] addObserver:self selector:@selector(hideAlertShadow:) name:@"willHideCPAlertShadow" object:nil];

    [theLoginWindow setAutoresizingMask:  CPViewMinXMargin | CPViewMaxXMargin | CPViewMinYMargin | CPViewMaxYMargin];

    [theLoginWindow orderFront:self];
    // Uncomment the following line to turn on the standard menu bar.
    //[CPMenu setMenuBarVisible:YES];
}
-(void)initialiseMainApplicationWindow:(CPNotification)aNotice{
    var theMainApplicationWindow = [[UOWMainApplicationWindow alloc] initWithContentRect:CGRectMakeZero() styleMask:CPBorderlessBridgeWindowMask],
        theMainApplicationWindowContentView = [theMainApplicationWindow contentView];
    CPAlertShadow = [[CPBox alloc] initWithFrame:[theMainApplicationWindowContentView frame]];

    [CPAlertShadow setFillColor:[CPColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.4]];
    [theMainApplicationWindowContentView addSubview:CPAlertShadow];

    [CPAlertShadow setHidden:YES];

    [theMainApplicationWindow setAutoresizingMask:  CPViewMinXMargin | CPViewMaxXMargin | CPViewMinYMargin | CPViewMaxYMargin];

    [theLoginWindow orderOut:self];
    [theMainApplicationWindow orderFront:self];
}
-(void)showAlertShadow:(CPNotification)aNotice{
    [CPAlertShadow setHidden:NO];
    console.log("Showing Shadow...");
}
-(void)hideAlertShadow:(CPNotification)aNotice{
    [CPAlertShadow setHidden:YES];
}

@end
